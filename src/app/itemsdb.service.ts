import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';;
import {ShoppingItem } from '../shoppingitem';

const dbName: string = 'items';

@Injectable({
  providedIn: 'root'
})

export class ItemsdbService {  
  constructor(private storage: Storage) { }

  
  getAll() {
    return new Promise((resolve, reject) => {
      this.storage.get(dbName).then(data => {
        resolve(data);
      }, error => {
        reject('Error retrieving data from storage');
      });
    });
  }

  save(items: Array<ShoppingItem>) {
    this.storage.set(dbName,JSON.stringify(items));
  }
}
