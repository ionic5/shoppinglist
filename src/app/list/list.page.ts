import { Component, OnInit, ViewChild } from '@angular/core';
import { ShoppingItem } from '../../shoppingitem';
import { ItemsdbService } from '../itemsdb.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  itemCount: number;
  items = Array<ShoppingItem>();
  newItem: string;
  newItemQuantity: any;
  @ViewChild('myInput',null) myInput;

  constructor(private itemsDbService: ItemsdbService) { }

  ngOnInit() {
    this.itemsDbService.getAll().then(data=> {
      this.items = JSON.parse(String(data));
      if (!this.items) {
        this.items = [];
      }
      this.itemCount = this.items.length;
    }, err => {
      console.log(err);
    });
  }

  addItem(event) {
    if (event.keyCode === 13) {
      const newShoppingItem = new ShoppingItem();
      newShoppingItem.name = this.newItem;
      newShoppingItem.quantity = this.newItemQuantity;
      this.items.push(newShoppingItem);
      this.newItem = '';
      this.newItemQuantity='';
      this.myInput.setFocus();
      this.itemCount = this.items.length;
      this.itemsDbService.save(this.items);
    }
  }

  removeItem(index: number) {
    this.items.splice(index, 1);
    this.itemCount = this.items.length;
    this.itemsDbService.save(this.items);
  }

}
