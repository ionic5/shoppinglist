import { TestBed } from '@angular/core/testing';

import { ItemsdbService } from './itemsdb.service';

describe('ItemsdbService', () => {
  let service: ItemsdbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemsdbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
